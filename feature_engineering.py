#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 14:53:41 2017

@author: shengnan
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import confusion_matrix, classification_report
import tensorflow as tf

root_path = os.getcwd()+'/Desktop/DS Project Data'
rawdata_path = root_path + '/Raw Data/'
data_path = root_path+'/Processed Data/'
model_path = root_path+'/Model Obj/'
o_path = root_path+'/Output/'


df = pd.read_csv(rawdata_path+'creditcard.csv')

### Data Exploration
print(df.groupby('Class').size())
#Class (very unbalanced)
#0    284315
#1       492
print(sum(df['Time']<60))   ###  remove 'Time'=0
df_orig = df.copy()
    
print(df.groupby('Class').agg({'Time':{'mean':lambda x: np.mean(x),'max':'max','min':'min'}}))
print(df.groupby('Class').agg({'Amount':{'mean':'mean','max':'max','min':'min','median':'median'}}))

plt.hist(df.loc[df['Class']==0,'Time'],bins=np.arange(0,180000,5000),normed=True)
plt.xticks(np.arange(0,50,2)*3600,np.arange(0,50,2))
plt.xlabel('Time(hour)')
plt.title('Normal')
plt.hist(df.loc[df['Class']==1,'Time'],bins=np.arange(0,180000,5000),normed=True)
plt.xticks(np.arange(0,50,2)*3600,np.arange(0,50,2))
plt.xlabel('Time(hour)')
plt.title('Fraud')

plt.hist(df.loc[df['Class']==0,'Amount'],bins=np.arange(0,1500,50),normed=True)
plt.xlabel('Amount')
plt.title('Normal')

plt.hist(df.loc[df['Class']==1,'Amount'],bins=np.arange(0,1500,50),normed=True)
plt.xlabel('Amount')
plt.title('Fraud')

####train test split
train, test = train_test_split(df,test_size=0.2)

# logistic regression
model_l = linear_model.LogisticRegression('l1')
X = train[['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
       'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
       'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount']]
Y = train['Class']
model_l.fit(X,Y)
pred1 = model_l.predict_proba(test[['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
       'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
       'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount']])
prediction1 = 1*(pd.DataFrame(pred1)[1]>0.1)
print(classification_report(test['Class'],prediction1))
print(confusion_matrix(test['Class'],prediction1))

#             precision    recall  f1-score   support
#
#          0       1.00      1.00      1.00     56871
#          1       0.80      0.81      0.81        91
#
#avg / total       1.00      1.00      1.00     56962
#
#[[56853    18]
# [   17    74]]
# 
model_l2 = linear_model.LogisticRegression('l1')
np.random.seed(250)
train_undersample = train[train['Class']==1].append(train[train['Class']==0].sample(401*10))
X = train_undersample[['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
       'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
       'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount']]
Y = train_undersample['Class']
model_l2.fit(X,Y)
pred2 = model_l2.predict_proba(test[['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
       'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
       'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount']])
prediction2 = 1*(pd.DataFrame(pred2)[1]>0.8)
print(classification_report(test['Class'],prediction2))
print(confusion_matrix(test['Class'],prediction2))
#             precision    recall  f1-score   support
#
#          0       1.00      1.00      1.00     56871
#          1       0.51      0.87      0.64        91
#
#avg / total       1.00      1.00      1.00     56962
#
#[[56795    76]
# [   12    79]]
X_test_orig=test[['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
       'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
       'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount']]
features = X.columns.values
for feature in features:
    mean, std = df[feature].mean(), df[feature].std()
    X.loc[:, feature] = (X[feature] - mean) / std
    X_test_orig.loc[:, feature] = (X_test_orig[feature] - mean) / std
    
Y = pd.concat([train['Class'],1-train['Class']],axis=1)
Y.columns = ['Fraud','Normal']
X_train = X.as_matrix()
Y_train = Y.as_matrix()
X_test = X_test_orig.as_matrix()
Y_test =  pd.concat([test['Class'],1-test['Class']],axis=1).as_matrix()
input_nodes = 30
mulitplier = 1.5 
# Number of nodes in each hidden layer
hidden_nodes1 = 18
hidden_nodes2 = round(hidden_nodes1 * mulitplier)
hidden_nodes3 = round(hidden_nodes2 * mulitplier)
pkeep = tf.placeholder(tf.float32)
# layer1
x = tf.placeholder(tf.float32, [None, input_nodes])
W1 = tf.Variable(tf.truncated_normal([input_nodes, hidden_nodes1], stddev = 0.15))
b1 = tf.Variable(tf.zeros([hidden_nodes1]))
y1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1)

# layer 2
W2 = tf.Variable(tf.truncated_normal([hidden_nodes1, hidden_nodes2], stddev = 0.15))
b2 = tf.Variable(tf.zeros([hidden_nodes2]))
y2 = tf.nn.sigmoid(tf.matmul(y1, W2) + b2)

# layer 3
W3 = tf.Variable(tf.truncated_normal([hidden_nodes2, hidden_nodes3], stddev = 0.15)) 
b3 = tf.Variable(tf.zeros([hidden_nodes3]))
y3 = tf.nn.sigmoid(tf.matmul(y2, W3) + b3)
y3 = tf.nn.dropout(y3, pkeep)

# layer 4
W4 = tf.Variable(tf.truncated_normal([hidden_nodes3, 2], stddev = 0.15)) 
b4 = tf.Variable(tf.zeros([2]))
y4 = tf.nn.softmax(tf.matmul(y3, W4) + b4)

# output
y = y4
y_ = tf.placeholder(tf.float32, [None, 2])

# Parameters
training_epochs = 10# should be 2000, it will timeout when uploading
training_dropout = 0.9
display_step = 1 # 10 
n_samples = Y_train.shape[0]
batch_size = 2048
learning_rate = 0.005
# Cost function: Cross Entropy
cost = -tf.reduce_sum(y_ * tf.log(y))

# We will optimize our model via AdamOptimizer
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

# Correct prediction if the most likely value (Fraud or Normal) from softmax equals the target value.
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

accuracy_summary = [] # Record accuracy values for plot
cost_summary = [] # Record cost values for plot
valid_accuracy_summary = [] 
valid_cost_summary = [] 
stop_early = 0 # To keep track of the number of epochs before early stopping

# Save the best weights so that they can be used to make the final predictions
checkpoint = model_path + "best_model.ckpt"
saver = tf.train.Saver(max_to_keep=1)

# Initialize variables and tensorflow session
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    
    for epoch in range(training_epochs): 
        for batch in range(int(n_samples/batch_size)):
            batch_x = X[batch*batch_size : (1+batch)*batch_size]
            batch_y = Y[batch*batch_size : (1+batch)*batch_size]

            sess.run([optimizer], feed_dict={x: batch_x, 
                                             y_: batch_y,
                                             pkeep: training_dropout})

        # Display logs after every 10 epochs
        if (epoch) % display_step == 0:
            train_accuracy, newCost = sess.run([accuracy, cost], feed_dict={x: X_train, 
                                                                            y_: Y_train,
                                                                            pkeep: training_dropout})

            valid_accuracy, valid_newCost = sess.run([accuracy, cost], feed_dict={x: X_test, 
                                                                                  y_: Y_test,
                                                                                  pkeep: 1})

            saver.save(sess, checkpoint)
            print ("Epoch:", epoch,
                   "Acc =", "{:.5f}".format(train_accuracy), 
                   "Cost =", "{:.5f}".format(newCost),
                   "Valid_Acc =", "{:.5f}".format(valid_accuracy), 
                   "Valid_Cost = ", "{:.5f}".format(valid_newCost))


# Find the predicted values, then use them to build a confusion matrix
predicted = tf.argmax(y, 1)
with tf.Session() as sess:  
    # Load the best weights
    saver.restore(sess, checkpoint)
    testing_predictions, testing_accuracy = sess.run([predicted, accuracy], 
                                                     feed_dict={x: X_test, y_:Y_test, pkeep: 1})
    
#    print("F1-Score =", f1_score(Y_test[:,1], testing_predictions))
    print("Testing Accuracy =", testing_accuracy)
#    print()
    c = confusion_matrix(Y_test[:,1], testing_predictions)
    show_confusion_matrix(c, ['Fraud', 'Normal'])

#array([[   81,    29],
#       [   14, 56838]])


















